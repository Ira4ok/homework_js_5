function User(firstName, lastName, birthDay) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.birthDay = birthDay;

    this.getLogin = () => {

      return (this.firstName.charAt(0) + this.lastName).toLowerCase();
    };

    this.setFirstName = (newName) => {

        Object.defineProperty(this, 'firstName', {
            writable: true
        });

        this.firstName = newName;

        Object.defineProperty(this, 'firstName', {
            writable: false
        });
    };

    this.setLastName = (newLastName) => {

        Object.defineProperty (this, 'lastName', {
            writable: true
        });

        this.lastName = newLastName;

        Object.defineProperty (this, 'lastName', {
            writable: false
        });
    };

    this.getAge = () => {

        let splitted = this.birthDay.split('.').reverse();
        let currentDate = new Date();
        console.log(currentDate);
        let age = currentDate.getFullYear() - splitted[0];
        let monthDifference = (currentDate.getMonth() + 1) - splitted[1];

        if (monthDifference < 0 || monthDifference === 0 && currentDate.getDate() < splitted[2]) {
            age --;
        }
        return age;
    };

    this.getPassword = () => {
        return (this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthDay.slice(6))
    }
}

let user = new User(prompt("Please enter your first name", "Irina"), prompt("Please enter your last name", "Kavun"), prompt("Please enter the date of your birth in format dd.mm.yyyy", "01.01.2001"));

user.setFirstName('Mariya');
user.setLastName('Kravetz');

console.log(user.getLogin());

console.log(user.getAge());
console.log(user.getPassword());








